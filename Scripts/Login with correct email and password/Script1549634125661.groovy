import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.youtube.com/')

WebUI.click(findTestObject('Object Repository/yt-formatted-string_Sign in'))

WebUI.setText(findTestObject('Object Repository/input_to continue to YouTube_i'), 'sharedfariz02@gmail.com')

WebUI.click(findTestObject('Object Repository/div_Learn more_ZFr60d CeoRYc'))

WebUI.setText(findTestObject('Page_YouTube/input_Too many failed attempts'), 'passwordlagi')

WebUI.click(findTestObject('Object Repository/span_Next'))

WebUI.click(findTestObject('Object Repository/img_Notifications_img'))

WebUI.verifyElementPresent(findTestObject('Object Repository/yt-formatted-string_Saringandu'), 0)

WebUI.closeBrowser()

